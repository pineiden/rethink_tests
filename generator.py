import asyncio
from networktools.dbs.rethinkdb import RethinkDB
from networktools.colorprint import bprint, rprint, gprint
from networktools.library import my_random_string
from datetime import tzinfo, timedelta, datetime

if __name__=="__main__":
    loop=asyncio.get_event_loop()
    #host='datawork.csn.uchile.cl'
    host='atlas.csn.uchile.cl'

    port=8080
    address=(host, port)
    kwargs={}
    kwargs.update({'address':address})
    code='HOLA'
    kwargs.update({'code':code})
    dbname='HOLA_HOLA'
    kwargs.update({'dbname':dbname})
    r=RethinkDB(**kwargs)
    def data_generator(id):
        return dict(id=id,
                    value=my_random_string(6),
                    dt=r.iso8601(datetime.utcnow().astimezone().isoformat()))

    async def rethink_generator(r_instance):
        r=r_instance
        await r.async_connect()
        await r.list_dbs()
        bprint("Conexión a Engine RehinkDB ok")
        server_info=await r.server_info()
        result=await r.create_db(dbname)
        await asyncio.sleep(1)
        gprint("Información del servidor %s" %server_info)
        await r.select_db(None)
        gprint("Database selected %s" %r.default_db)
        table_name="HOLA_TABLE"
        result=await r.create_table(table_name, r.default_db)
        print("Table created")
        await asyncio.sleep(1)
        await r.list_tables(r.default_db)
        for id in range(180):
            new_data=data_generator(id)
            bprint("Sending %s" %new_data)
            result_insert=await r.save_data(table_name, new_data)
            await asyncio.sleep(1)

        await asyncio.sleep(1)
        del_db=await r.delete_db(r.default_db)
    loop.run_until_complete(rethink_generator(r))



