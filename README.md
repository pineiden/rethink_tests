Rethink TESTS
============

Those scripts allow you to test the rethinkdb python module:

basic.py
=======

Runs normal functions

async_test.py
============

Runs async functions nested on the RethinkDB class from [NetworkTools](https://gitlab.com/pineiden/networktools/tree/master/networktools)

generator.py
===========

Create a bunch of data and send sequentialy to database.

monitor.py
=========

Receive the signal about there are new data and obtain that

Monitor depends also from [TaskTools](https://gitlab.com/pineiden/tasktools) module to run an async loop.

Try generator in a terminal and also monitor in another. RethinkDB must register the data and monitor collect that.
