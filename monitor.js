let r =  require('rethinkdb')
let host='datawork.csn.uchile.cl'
let port=28015
let dbname='datawork'

let info={host:host,port:port,db:dbname}
let table_name='AEDA_GSOF'



function get_datetime_di(delta){
  let d = new Date();
  d.setSeconds(d.getSeconds()-delta)
  var n=d.toISOString();
  return r.ISO8601(n)
}

let promise=r.connect(info, function(err,conn){
  if(err) throw err;
  conn.addListener('error', function(e){
    console.log("Error de conexión")
  })
  conn.addListener('close', function(e){
    console.log("Conexión cerrada")
  })
  console.log("Server info:")
  conn.server(function(err, info){console.log(info)})

  let delta=300
  let key='DT_GEN'
  let di=get_datetime_di(delta)

  console.log(di)

  r.db(dbname).tableList().run(conn, function(err,data){console.log(data)})
  r.db(dbname).table(table_name).filter(r.row(key).gt(di)).run(conn, function(err, data){
    console.log(data)})


})
console.log("Resultado")
console.log(promise)
