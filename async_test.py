import asyncio
from networktools.dbs.rethinkdb import RethinkDB
from networktools.colorprint import bprint, rprint, gprint

if __name__=='__main__':
    loop=asyncio.get_event_loop()
    host='datawork.csn.uchile.cl'
    port=8080
    address=(host, port)
    kwargs={}
    kwargs.update({'address':address})
    code='HOLA'
    kwargs.update({'code':code})
    dbname='HOLA_HOLA'
    kwargs.update({'dbname':dbname})
    r=RethinkDB(**kwargs)
    async def rethink_script(r_instance):
        r=r_instance
        await r.async_connect()
        await r.list_dbs()
        bprint("Conexión a Engine RethinkDB ok")
        server_info=await r.server_info()
        gprint("Información del servidor %s" % server_info)
        result=await r.create_db(dbname)
        rprint("Database %s creada correctamente %s" %(dbname, result))
        bprint("Default db %s" %r.default_db)
        await r.select_db(None)
        gprint("Database selected %s" % r.default_db)
        table_name_h='HOLA_TABLE'
        result = await r.create_table(table_name_h,r.default_db)
        await asyncio.sleep(1)
        bprint("Tables on database %s" %r.tables)
        rprint("Table %s creada" %table_name_h)

        await asyncio.sleep(1)
        table_name_ch="CHAO_TABLE"
        result= await r.create_table(table_name_ch,r.default_db)
        rprint("Table %s creada" %table_name_ch)
        list_tables=await r.list_tables(r.default_db)
        bprint("List of tables %s" %list_tables)
        # Send data
        data={'features': [{'geometry': {'coordinates': [-0.011478945016798775,
                                                          -0.07493812625138219,
                                                          -0.03889225505535643],
                                         'type': 'Point'},
                            'properties': {'coordinateType': 'NEU'},
                            'type': 'Feature'}],
              'properties': {'EError': 0.001139707213682439,
                             'NError': 0.00035648577208169976,
                             'SNCL': 'VALN.FU.LY_.00',
                             'UError': 0.0002621995070107351,
                             'quality': 100,
                             'sampleRate': 1,
                             'station': 'VALN',
                             'time': 1511464818015},
              'type': 'FeatureCollection'}

        bprint("Data to send %s" %data)
        result_insert_h=await r.save_data(table_name_h, data)
        result_insert_ch=await r.save_data(table_name_ch, data)
        await asyncio.sleep(20)
        rprint("Borrando database")
        del_db=await r.delete_db(r.default_db)


    loop.run_until_complete(rethink_script(r))
