from networktools.colorprint import bprint, rprint, gprint
import rethinkdb as r

if __name__=='__main__':
    host='datawork.csn.uchile.cl'
    port=80

    #Connect to rethinkdb...
    conn = r.connect(host=host)

    #Show server info

    sinfo=conn.server()

    bprint("Info de servidor rethinkdb %s" % sinfo)

    #Create new database

    dbname="HOLA_HOLA"

    db_list=r.db_list().run(conn)

    if not dbname in db_list:
        result_new=r.db_create(dbname).run(conn)
    else:
        rprint("Ya existe esa database")

    #set database to use
    conn.use(dbname)

    #Create table
    table_name_h="HOLA_TABLE"
    result_ctable_h=r.db(dbname).table_create(table_name_h).run(conn)

    table_name_ch="CHAO_TABLE"
    result_ctable_ch=r.db(dbname).table_create(table_name_ch).run(conn)

    gprint("Result create table hola %s" %result_ctable_h)
    gprint("Result create table chao %s" %result_ctable_ch)
    
    #LIST OF TABLES

    list_tables=r.db(dbname).table_list().run(conn)

    bprint("List of tables %s" %list_tables)

    # Send data
    data={'features': [{'geometry': {'coordinates': [-0.011478945016798775,
                                                      -0.07493812625138219,
                                                      -0.03889225505535643],
                                     'type': 'Point'},
                        'properties': {'coordinateType': 'NEU'},
                        'type': 'Feature'}],
          'properties': {'EError': 0.001139707213682439,
                         'NError': 0.00035648577208169976,
                         'SNCL': 'VALN.FU.LY_.00',
                         'UError': 0.0002621995070107351,
                         'quality': 100,
                         'sampleRate': 1,
                         'station': 'VALN',
                         'time': 1511464818015},
          'type': 'FeatureCollection'}

    bprint("Data to send %s" %data)

    result_insert=r.table(table_name_h).insert(data).run(conn)
    result_insert=r.table(table_name_ch).insert(data).run(conn)

    # Delete database test
    import time

    time.sleep(10)

    del_db=r.db_drop(dbname).run(conn)

