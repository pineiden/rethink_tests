import asyncio
import functools
from networktools.dbs.rethinkdb import RethinkDB
from networktools.colorprint import bprint, rprint, gprint
from networktools.library import my_random_string

from tasktools.taskloop import coromask, renew, simple_fargs, simple_fargs_out

from datetime import tzinfo, timedelta, datetime

import rethinkdb as rdb
import pytz

if __name__=="__main__":
    loop=asyncio.get_event_loop()
    dbsource={
        'collector':dict(
            host='atlas.csn.uchile.cl',
            dbname='collector',
            table_name="AEDA_GSOF",
            key='DT_GEN'),
        'datawork':dict(
            host='datawork.csn.uchile.cl',
            dbname='datawork',
            table_name="HSCO_GSOF",
            key='DT_GEN'),
        'test':dict(
            host='atlas.csn.uchile.cl',
            dbname='HOLA_HOLA',
            table_name="HOLA_TABLE",
            key='dt'),
    }
    option='datawork'

    host=dbsource[option]['host']
    dbname=dbsource[option]['dbname']
    table_name=dbsource[option]['table_name']
    key=dbsource[option]['key']

    port=8080
    address=(host, port)
    kwargs={}
    kwargs.update({'address':address})
    code='HOLA'
    kwargs.update({'code':code})
    kwargs.update({'dbname':dbname})
    r=RethinkDB(**kwargs)

    def get_datetime_di(delta:int=600):
        df=datetime.now(tz=pytz.utc)
        di=df+timedelta(seconds=-delta)
        return di.astimezone().isoformat()


    async def new_data(r_instance, last_date=None):
        r=r_instance
        control=False
        di=None

        if not last_date:
            di=get_datetime_di()
            #extract from data --> di
        else:
            di=last_date
            control=True

        await r.async_connect()
        await r.list_dbs()
        bprint("Conexión a Engine RehinkDB ok")
        server_info=await r.server_info()
        await asyncio.sleep(.1)
        gprint("Información del servidor %s" %server_info)
        await r.select_db(None)
        await r.get_indexes(table_name)
        gprint("Database selected %s" %r.default_db)
        bprint(r.dblist)
        #result = await r.set_change_handler(table_name)
        #print(result)
        await r.list_tables(r.default_db)
        if not control:
            cursor=await r.get_data_filter(table_name, r.iso8601(di),key)
        else:
            cursor=await r.get_data_filter(table_name, di,key)

        print(cursor)
        await asyncio.sleep(.3)
        if cursor:
            df=cursor[-1][key]
        else:
            df=di
        return [r, df]

    def task_new_data():
        args=[r, None]
        try:
            task=loop.create_task(
                coromask(
                    new_data,
                    args,
                    simple_fargs_out
                )
            )
            task.add_done_callback(
                functools.partial(
                    renew,
                    task,
                    new_data,
                    simple_fargs_out
                )
            )
        except Exception as exec:
            raise exec
        if not loop.is_running():
            loop.run_forever()

    task_new_data()


    
